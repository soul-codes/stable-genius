import { defaultKeyGenerator } from "./defaultKeyGenerator";

/**
 * Returns a unique key-generating function that returns a stable value for
 * each object argument. The key is guaranteed to be unique among the set of
 * objects ever passed the key-generating function, as long as the keyGenerator
 * always generates a unique value.
 *
 * @param tag Adds a tag that serves as a diagnostic to
 * the unique key. The tag can be used to create a namespace distinguishing
 * keys generated by different factories.
 *
 * @param keyGenerator Defaults to an autoincrementing number function.
 */
export function weakKeyFactory<T extends object>(
  tag?: (((object: T) => string) | string) | null,
  keyGenerator?: (tag: string) => string
) {
  const keys = new WeakMap<T, string>();
  const keyFn = keyGenerator || defaultKeyGenerator;
  return function weakKey(value: T) {
    const key =
      keys.get(value) ||
      keyFn(tag ? (typeof tag === "function" ? tag(value) : tag) : "");
    keys.set(value, key);
    return key;
  };
}
