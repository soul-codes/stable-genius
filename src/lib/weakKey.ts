import { weakKeyFactory } from "./weakKeyFactory";

/**
 * The default weak key factory.
 */
export const weakKey = weakKeyFactory();
