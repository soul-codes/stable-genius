/**
 * The default unique key generator which simply crates an auto-incrementing
 * number prefixed with `$` and suffixed with the tag.
 */
export function defaultKeyGenerator(tag: string) {
  return "$" + ++universalCounter + (tag ? "_" + tag : "");
}

let universalCounter = 0;
