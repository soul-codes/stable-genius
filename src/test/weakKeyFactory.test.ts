import { weakKeyFactory } from "@lib";

test("stability", () => {
  const weakKey = weakKeyFactory();
  const obj1 = {};
  const obj2 = {};
  const obj3 = {};
  expect(weakKey(obj1)).not.toEqual(weakKey(obj2));
  expect(weakKey(obj2)).not.toEqual(weakKey(obj3));
  expect(weakKey(obj3)).not.toEqual(weakKey(obj1));
});

test("uniqueness", () => {
  const weakKey = weakKeyFactory();
  const obj1 = {};
  const obj2 = {};
  const obj3 = {};
  expect(weakKey(obj1)).not.toEqual(weakKey(obj2));
  expect(weakKey(obj2)).not.toEqual(weakKey(obj3));
  expect(weakKey(obj3)).not.toEqual(weakKey(obj1));
});

test("custom key generator.", () => {
  const pseudoRandomKeys = (function* generateKeys() {
    yield "key1";
    yield "key2";
    yield "key3";
  })();

  const weakKey = weakKeyFactory(
    "myTag",
    (tag) => `${tag}_${pseudoRandomKeys.next().value!}`
  );
  expect(weakKey({})).toEqual("myTag_key1");
  expect(weakKey({})).toEqual("myTag_key2");
  expect(weakKey({})).toEqual("myTag_key3");
});

test("custom tag", () => {
  const pseudoRandomKeys = (function* generateKeys() {
    yield "key1";
    yield "key2";
    yield "key3";
  })();

  const weakKey = weakKeyFactory(
    (obj: { foo: number }) => "foo:" + obj.foo,
    (tag) => `${tag}_${pseudoRandomKeys.next().value!}`
  );
  expect(weakKey({ foo: 5 })).toEqual("foo:5_key1");
  expect(weakKey({ foo: 4 })).toEqual("foo:4_key2");
  expect(weakKey({ foo: 3 })).toEqual("foo:3_key3");
});
